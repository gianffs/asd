
const Login = (props) => {
  return (
    <>
      <h1 className="text-center
      font-bold
      tracking-wide
      mt-20
      mb-12
      text-4xl">SERSINCA</h1>


      <div className="bg-red-100 mx-auto max-w-md py-10 px-10 shadow-mb rounded-lg">
        <h1 className="text-center font-semibold text-2xl my-8">Login</h1>
        <input className="shadow appearance-none border rounded w-full py-3 px-3 mb-6 leading-tight focus:outline-none focus:shadow-outline" 
                type="text" 
                placeholder="Username" 
                required>
        </input>
        <input className="shadow appearance-none border rounded w-full py-3 px-3 leading-tight focus:outline-none focus:shadow-outline" 
                type="text" 
                placeholder="Password" 
                required>
        </input>
        <div className="flex flex-col">
          <button className="bg-blue-500 py-2 mt-12 rounded shadow-md text-white font-semibold hover:bg-blue-600">Register</button>
        </div>
      </div>
      <p className="text-center text-gray-500 text-xs mt-5 mb-5">2022 Sersinca C.A, All rights reserved.</p>
      
    </>
  )
}

export default Login
